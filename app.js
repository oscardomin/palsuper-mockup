/*
 * File: app.js
 *
 * This file was generated by Sencha Architect version 3.0.2.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Sencha Touch 2.2.x library, under independent license.
 * License of Sencha Architect does not include license for Sencha Touch 2.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

// @require @packageOverrides
Ext.Loader.setConfig({

});


Ext.application({
    models: [
        'Contact',
        'Group',
        'List'
    ],
    stores: [
        'MyJsonStoreContacts',
        'MyJsonStoreGroup',
        'MyJsonStoreMyLists',
        'MyJsonStoreAnaBotella',
        'MyJsonStoreMadrid'
    ],
    views: [
        'MainTab',
        'HomePanel',
        'MisListas',
        'Comprar',
        'ConsumoPropio',
        'Notificaciones',
        'CrearLista',
        'AnadirProductos',
        'Settings',
        'EnumListas',
        'Cafes',
        'Carnes',
        'Chocolates',
        'Frutas_y_Verduras',
        'Higiene',
        'Lacteos',
        'Pescado_y_Marisco',
        'Conservas',
        'Perfil',
        'FamiliaProdZoom',
        'MensualZoom',
        'Pasta',
        'Snacks',
        'Aceites',
        'MyNavigationView',
        'AnadirPart',
        'IniciarCompraAna',
        'Pagar',
        'IniciarCompraMadrid',
        'EditMadrid',
        'EditAna'
    ],
    controllers: [
        'MainNavController'
    ],
    name: 'MyApp',

    launch: function() {

        Ext.create('MyApp.view.MainTab', {fullscreen: true});
    }

});
