/*
 * File: app/view/CrearLista.js
 *
 * This file was generated by Sencha Architect version 3.0.2.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Sencha Touch 2.2.x library, under independent license.
 * License of Sencha Architect does not include license for Sencha Touch 2.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyApp.view.CrearLista', {
    extend: 'Ext.Panel',
    alias: 'widget.crearlista',

    requires: [
        'Ext.Spacer',
        'Ext.field.TextArea',
        'Ext.field.DatePicker',
        'Ext.picker.Date',
        'Ext.Button'
    ],

    config: {
        html: '',
        layout: 'vbox',
        scrollable: 'vertical',
        items: [
            {
                xtype: 'spacer'
            },
            {
                xtype: 'textfield',
                label: 'Título',
                labelAlign: 'top',
                labelWidth: '40%'
            },
            {
                xtype: 'textareafield',
                label: 'Descripción',
                labelAlign: 'top',
                labelWidth: '40%'
            },
            {
                xtype: 'datepickerfield',
                label: 'Fecha',
                labelAlign: 'top',
                labelWidth: '40%',
                placeHolder: 'mm/dd/yyyy',
                picker: {
                    dayText: 'Dia',
                    monthText: 'Mes',
                    slotOrder: [
                        'day',
                        'month',
                        'year'
                    ],
                    yearFrom: 2013,
                    yearText: 'Año',
                    yearTo: 2020,
                    doneButton: {
                        ui: 'confirm-round',
                        text: 'OK'
                    },
                    cancelButton: {
                        ui: 'decline-round',
                        text: 'Cancelar'
                    }
                }
            },
            {
                xtype: 'button',
                id: 'anadirpart',
                iconCls: 'add',
                text: 'Añadir participantes'
            },
            {
                xtype: 'spacer',
                id: ''
            },
            {
                xtype: 'container',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'button',
                        id: 'oknewlist',
                        ui: 'confirm-round',
                        width: 125,
                        badgeText: '',
                        iconAlign: 'right',
                        iconCls: 'add',
                        text: 'Crear'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'button',
                        id: 'cancelnewlist',
                        ui: 'decline-round',
                        width: 125,
                        text: 'Cancelar'
                    },
                    {
                        xtype: 'spacer'
                    }
                ]
            },
            {
                xtype: 'spacer'
            }
        ]
    }

});