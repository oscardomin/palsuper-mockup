/*
 * File: app/view/HomePanel.js
 *
 * This file was generated by Sencha Architect version 3.0.2.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Sencha Touch 2.2.x library, under independent license.
 * License of Sencha Architect does not include license for Sencha Touch 2.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyApp.view.HomePanel', {
    extend: 'Ext.form.Panel',
    alias: 'widget.homepanel',

    requires: [
        'Ext.Spacer',
        'Ext.Container',
        'Ext.Button'
    ],

    config: {
        ui: 'light',
        scrollable: false,
        layout: {
            type: 'vbox',
            pack: 'center'
        },
        items: [
            {
                xtype: 'spacer',
                height: ''
            },
            {
                xtype: 'container',
                layout: {
                    type: 'hbox',
                    pack: 'center'
                },
                items: [
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'button',
                        height: 100,
                        html: '<img width=100% height=100% src=images/my_lists.png></img>',
                        id: 'mislistas',
                        padding: 0,
                        ui: 'plain',
                        width: 100,
                        text: 'MyButton14'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'button',
                        height: 100,
                        html: '<img width=100% height=100% src=images/buy.png></img>',
                        id: 'comprar',
                        padding: 0,
                        ui: 'plain',
                        width: 100,
                        text: 'MyButton14'
                    },
                    {
                        xtype: 'spacer'
                    }
                ]
            },
            {
                xtype: 'spacer'
            },
            {
                xtype: 'container',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'button',
                        height: 100,
                        html: '<img width=100% height=100% src=images/icon_chart.png></img>',
                        id: 'miconsumo',
                        padding: 0,
                        ui: 'plain',
                        width: 100,
                        text: 'MyButton14'
                    },
                    {
                        xtype: 'spacer'
                    },
                    {
                        xtype: 'button',
                        height: 100,
                        html: '<img width=100% height=100% src=images/alert_icon.png></img>',
                        id: 'alertas',
                        padding: 0,
                        ui: 'plain',
                        width: 100,
                        badgeText: '2',
                        text: ''
                    },
                    {
                        xtype: 'spacer'
                    }
                ]
            },
            {
                xtype: 'spacer'
            }
        ]
    }

});